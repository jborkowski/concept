package com.jobo

import java.net.URL

import akka.actor.{ActorSystem, PoisonPill}
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import com.jobo.actors.{Start, Supervisor}
import com.jobo.http.routes.DealServiceRoute
import com.jobo.services.{CouponService, DealService, VendorService}
import com.jobo.utils.{Config, DatabaseService, FlywayService}

import scala.concurrent.Await
import scala.concurrent.duration._

object CouponFetcher extends App with Config {
  implicit val system = ActorSystem()
  implicit val executor = system.dispatcher
  implicit val materializer = ActorMaterializer()

  val flywayService = new FlywayService(jdbcUrl, dbUser, dbPassword)
  flywayService.migrateDatabaseSchema

  val databaseService = new DatabaseService(jdbcUrl, dbUser, dbPassword)
  val dealService = DealService(databaseService)
  val vendorService = VendorService(databaseService)
  val couponService = CouponService(databaseService)

  val httpService = new DealServiceRoute(couponService)
  Http().bindAndHandle(httpService.routes, httpHost, httpPort)

  val supervisor = system.actorOf(Supervisor.props(system, vendorService, dealService))

  supervisor ! Start

  Await.result(system.whenTerminated, 10 minutes)

  supervisor ! PoisonPill
  system.terminate

}

