package com.jobo.services

import com.jobo.actors.Deal
import com.jobo.models._
import com.jobo.utils.DatabaseService

import scala.concurrent.{ExecutionContext, Future}

case class CouponService(databaseService: DatabaseService)(implicit executionContext: ExecutionContext)
  extends VendorTable with DealTable {
  import databaseService._
  import databaseService.driver.api._

  def getCoupons(): Future[Seq[Coupon]] = {
    val findVendorsWithDealsQuery = deals
      .join(vendors).on(_.vendorId === _.id)
      .result
    val action = for {
      result <- findVendorsWithDealsQuery
    } yield {
      result.map { row =>
        val vendorsTableRow = row._2
        val dealsTableRow = row._1

        Coupon(name = vendorsTableRow.name, url = vendorsTableRow.url, couponName = dealsTableRow.name,
          couponUrl = dealsTableRow.url, couponImgUrl = dealsTableRow.imgUrl, updatedAt = dealsTableRow.updatedAt)
      }
    }

    db.run(action)
  }
}


case class VendorService(databaseService: DatabaseService)(implicit executionContext: ExecutionContext)
  extends VendorTable with DealTable {
  import databaseService._
  import databaseService.driver.api._

  def getVendors(): Future[Seq[VendorEntity]] = db.run(vendors.result)

  def getVendorsWithDeals(): Future[Seq[Coupon]] = {
    val findVendorsWithDealsQuery = deals
          .join(vendors).on(_.vendorId === _.id)
          .result
        val action = for {
          result <- findVendorsWithDealsQuery
        } yield {
          result.map { row =>
            val vendorsTableRow = row._2
            val dealsTableRow = row._1

            Coupon(name = vendorsTableRow.name, url = vendorsTableRow.url, couponName = dealsTableRow.name,
              couponUrl = dealsTableRow.url, couponImgUrl = dealsTableRow.imgUrl, updatedAt = dealsTableRow.updatedAt)
          }
        }

        db.run(action)
  }

  //def getDealsByVendorId(id: Long): Future[Option[Seq[DealEntity]]] = db.run(deals.filter(_.id === id).result)

  //def createVendor(vendor: VendorEntity): Future[VendorEntity] = db.run(vendors returning vendors += vendor)
}

case class DealService(databaseService: DatabaseService)(implicit executionContext: ExecutionContext) extends DealTable {
  import databaseService._
  import databaseService.driver.api._

  def getDeals(): Future[Seq[DealEntity]] = db.run(deals.result)

  def getDealWithVendor() ={

  }

  def getDealsByVendorId(id: Int): Future[Seq[DealEntity]] = db.run(deals.filter(_.vendorId === id).result)

  def getDealByNameAndVendorId(name: String, vendorId: Int): Future[Seq[DealEntity]] = {
    val q1 = deals.filter(_.vendorId === vendorId)
    val q2 = deals.filter(_.name === name)
    db.run((q1 ++ q2).result)
  }
  //db.run(deals.filter(_.vendorId === vendorId).filter(_.name === name))

  def createDeal(deal: DealEntity): Future[DealEntity] = db.run(deals returning deals += deal)

  //  def updateDeal(id: Long, updatedDeal: DealEntityUpdate): Future[Option[DealEntity]] = getUserById(id).flatMap {
  //    case Some(user) =>
  //      val updatedUser = userUpdate.merge(user)
  //      db.run(users.filter(_.id === id).update(updatedUser)).map(_ => Some(updatedUser))
  //    case None => Future.successful(None)
  //  }

  def deleteDeal(id: Long): Future[Int] = db.run(deals.filter(_.id === id).delete)

}