package com.jobo.models

import com.jobo.utils.DatabaseService
import org.joda.time.LocalDateTime


trait DealTable {
  protected val databaseService: DatabaseService
  import databaseService.driver.api._

  class Deal(tag: Tag) extends Table[DealEntity](tag, "deals") {
    def id = column[Option[Long]]("id", O.PrimaryKey, O.AutoInc)
    def vendorId = column[Int]("vendor_id")
    def name = column[String]("name")
    def url = column[Option[String]]("url")
    def imgUrl = column[Option[String]]("imgurl")
    def updatedAt = column[LocalDateTime]("updated_at")

    def * = (id, vendorId, name, url, imgUrl, updatedAt) <> ((DealEntity.apply _).tupled, DealEntity.unapply)
  }

  protected val deals = TableQuery[Deal]
}

case class DealEntity(id: Option[Long] = None, vendorId: Int, name: String, url: Option[String] = None, imgUrl: Option[String] = None, updatedAt: LocalDateTime = LocalDateTime.now()) {
  require(!name.isEmpty, "name.empty")
  //require(!vendorId, "vendorId.empty")
}