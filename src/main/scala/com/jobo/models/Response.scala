package com.jobo.models

import org.joda.time.LocalDateTime

case class Coupon(name: String, url: String, couponName: String,
                  couponUrl: Option[String] = None, couponImgUrl: Option[String] = None, updatedAt: LocalDateTime)