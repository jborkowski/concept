package com.jobo.models

import com.jobo.utils.{Config, DatabaseService}
import org.joda.time.LocalDateTime

import scala.concurrent.{ExecutionContext, Future}

trait VendorTable {
  protected val databaseService: DatabaseService
  import databaseService.driver.api._

  class Vendor(tag: Tag) extends Table[VendorEntity](tag, "vendors") {
    def id = column[Option[Int]]("id", O.PrimaryKey, O.AutoInc)
    def url = column[String]("url")
    def name = column[String]("name")
    def select = column[String]("select")
    def regex = column[String]("regex")
    def updatedAt = column[LocalDateTime]("updated_at")

    def * = (id, url, name, select, regex, updatedAt) <> ((VendorEntity.apply _).tupled, VendorEntity.unapply)
  }

  protected val vendors = TableQuery[Vendor]
}

case class VendorEntity(id: Option[Int] = None, url: String, name: String, select: String, regex: String, updatedAt: LocalDateTime) {
  require(!url.isEmpty, "url.empty")
  require(!name.isEmpty, "name.empty")
  require(!select.isEmpty, "select.empty")
  require(!regex.isEmpty, "regex.empty")
}