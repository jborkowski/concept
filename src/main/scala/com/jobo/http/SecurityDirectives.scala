package com.jobo.http

import akka.http.scaladsl.server.Directive1

trait SecurityDirectives {

  import akka.http.scaladsl.server.directives.BasicDirectives._
  import akka.http.scaladsl.server.directives.HeaderDirectives._
  import akka.http.scaladsl.server.directives.RouteDirectives._
  import akka.http.scaladsl.server.directives.FutureDirectives._

//  def authenticate: Directive1[UserEntity] = {
//    headerValueByName("Token").flatMap { token =>
//      onSuccess(authService.authenticate(token)).flatMap {
//        case Some(user) => provide(user)
//        case None       => reject
//      }
//    }
//  }
//
//  protected val authService: AuthService
}
