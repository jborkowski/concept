package com.jobo.http.routes

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.server.Directives
import com.jobo.models.{Coupon, DealEntity}
import com.jobo.services.{CouponService}
import org.joda.time.{DateTime, LocalDateTime}
import spray.json.{DefaultJsonProtocol, JsString, JsValue, RootJsonFormat, deserializationError}

import scala.concurrent.ExecutionContext

trait Protocols extends DefaultJsonProtocol with SprayJsonSupport {

  implicit val jodaLocalDateTimeFormat = new RootJsonFormat[LocalDateTime] {
    def write(value: LocalDateTime) = JsString(value.toString)
    def read(value: JsValue): LocalDateTime = value match {
      case JsString(s) =>
        try DateTime.parse(s).toLocalDateTime
        catch {
          case e: Throwable => deserializationError("Couldn't convert '" + s + "' to a local date-time: " + e.getMessage)
        }
      case s => deserializationError("Couldn't convert '" + s + "' to a local date-time")
    }
  }

  implicit val dealFormat = jsonFormat6(DealEntity)
  implicit val couponFormat = jsonFormat6(Coupon)
}

class DealServiceRoute(couponService: CouponService)(implicit executionContext: ExecutionContext)
   extends Directives with Protocols {

  import couponService._

  val routes = pathPrefix("coupons") {
    pathEndOrSingleSlash {
      complete(
        getCoupons()
      )
    }
  }
}



