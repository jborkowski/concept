package com.jobo.actors

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.pattern.{ask, pipe}
import akka.util.Timeout
import com.jobo.utils.Config

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.concurrent.duration._

class SiteFetcher(supervisor: ActorRef, indexer: ActorRef) extends Actor with ActorLogging with Config {
  val process = "process this"
	val worker = context actorOf Worker.props(indexer)
  var localVendor: Vendor = _

	implicit val timeout = Timeout(1 minutes)
	val tick = context.system.scheduler.schedule(0 millis, interval minutes, self, process)

  override def receive: Receive = {
    case Fetch(vendor) =>
      localVendor = vendor

    case `process` =>
      log.info("coupon fetching... {}", localVendor.name)
      (worker ? Fetch(localVendor))
        .mapTo[FetchFinished]
        .recoverWith { case e => Future { FetchFailure(localVendor, e) } }
        .pipeTo(supervisor)
  }
}

object SiteFetcher {
  def props(supervisor: ActorRef, indexer: ActorRef) = Props(classOf[SiteFetcher], supervisor, indexer)
}
