package com.jobo.actors

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import org.jsoup.Jsoup


class Worker(indexer: ActorRef) extends Actor with ActorLogging {
	
	override def receive: Receive = {
		case Fetch(vendor) =>
			val dealsArray = getCoupon(vendor)
      dealsArray foreach { item =>
        log.info(item.name)
      }
			sender() ! FetchFinished(vendor.id, dealsArray)
			indexer ! Index(vendor.id, dealsArray)
	}

  def getCoupon(vendor: Vendor): Array[Deal] = {
    import scala.collection.JavaConverters._
    val response = Jsoup.connect(vendor.url)
      .ignoreContentType(true)
      .userAgent("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) " +
        "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36")
      .execute()
    val contentType: String = response.contentType
    if (contentType.startsWith("text/html")) {
      response
        .parse()
        .select(vendor.select)
        .asScala
        .map(_.toString.replaceAll("\n", ""))
        .map {
          case vendor.regex(url, imgUrl, name) => Deal(name, Some(url), Some(imgUrl))
          case vendor.regex(name) => Deal(name)
          case vendor.regex(imgUrl, name) => Deal(name = name, imgUrl = Some(imgUrl))
//          case _ => Nil
        }
        .toArray
    } else {
      Array.empty[Deal]
    }
  }
}

object Worker {
	def props(indexer: ActorRef) = Props(classOf[Worker], indexer) 
}