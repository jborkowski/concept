package com.jobo.actors

import akka.actor.{Actor, ActorLogging, ActorRef, ActorSystem, Props}
import com.jobo.services.{DealService, VendorService}

class Supervisor(system: ActorSystem, vendorService: VendorService, dealService: DealService) extends Actor with ActorLogging {
  implicit val executor = context.dispatcher
  val indexer = system.actorOf(Indexer.props(self, dealService))

  var counter = 0
  var fetchCounts = Map.empty[Int, Int]
  var vendorId2Actor = Map.empty[Int, ActorRef]

  val maxRetries = 2

  override def receive: Receive = {
    case Start =>
      vendorService.getVendors().onSuccess {
        case vArr => vArr.foreach { v =>
          val tmp = Vendor(id = v.id.getOrElse(-1), name = v.name, url = v.url, select = v.select, regex = v.regex.r)
          start(tmp)
        }
      }
    case FetchFinished(vendorId, deals) =>
      log.info("Received new Coupon {}", deals.headOption.getOrElse(Deal("ohh")).name)
      counter += 1
    case FetchFailure(vendor, reason) =>
      val retries: Int = fetchCounts(vendor.id)
      log.info(s"scraping failed ${vendor.name}, $retries, reason = $reason")
      if (retries < maxRetries) {
        //countVisits(url)
        vendorId2Actor(vendor.id) ! Fetch(vendor)
      }
        //checkAndShutdown(url)
  }

  def start(vendor: Vendor) = {
    val vendorId = vendor.id
    log.info("VendorId = {}", vendorId)

    var actor  = vendorId2Actor.getOrElse(vendorId, {
      val tmpRef = system.actorOf(SiteFetcher.props(self, indexer))
      vendorId2Actor += (vendorId -> tmpRef)
      tmpRef
    })

    actor ! Fetch(vendor)
  }
}

object Supervisor {
  def props(system: ActorSystem, vendorService: VendorService, dealService: DealService): Props =
    Props(classOf[Supervisor], system, vendorService, dealService)
}
