package com.jobo.actors

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import com.jobo.models.DealEntity
import com.jobo.services.DealService

import scala.concurrent.Future

class Indexer(supervisor: ActorRef, dealService: DealService) extends Actor with ActorLogging {
	implicit val executor = context.dispatcher

  override def receive: Receive = {
		case Index(vendorId, deals) =>
			log.info("Saving coupon from {}", vendorId)
			deals
				.distinct
				.foreach { item =>
          dealService.getDealByNameAndVendorId(name = item.name, vendorId = vendorId).flatMap {
          case Nil =>
            dealService.createDeal(
              DealEntity(vendorId = vendorId, name = item.name, url = item.url, imgUrl = item.imgUrl))
          case c =>
            log.info("Coupon already exists")
            Future.successful(c)
        }
			}

			supervisor ! IndexFinished(vendorId)
	}

	@throws[Exception](classOf[Exception]) 
	override def postStop(): Unit = {
	 		super.postStop()
	 }
}

object Indexer {
	def props(supervisor: ActorRef, dealService: DealService) = Props(classOf[Indexer], supervisor, dealService)
}