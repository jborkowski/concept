package com.jobo.actors

import scala.util.matching.Regex

case class Vendor(id: Int, url: String, name: String, select: String, regex: Regex)
case class Deal(name: String, url: Option[String] = None, imgUrl: Option[String] = None)

case object Start

case class Fetch(vendor: Vendor)
case class FetchFinished(vendorId: Int, deals: Array[Deal])
case class FetchFailure(vendor: Vendor, e: Throwable)

case class Index(vendorId: Int, deals: Array[Deal])
case class IndexFinished(vendorId: Int)