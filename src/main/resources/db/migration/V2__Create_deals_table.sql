CREATE TABLE "deals" (
  "id"          BIGSERIAL PRIMARY KEY,
  "vendor_id"   INTEGER NOT NULL,
  "name"        VARCHAR NOT NULL,
  "url"         VARCHAR,
  "imgurl"      VARCHAR,
  "updated_at"  TIMESTAMP NOT NULL,
  CONSTRAINT "deals_vendor_id_fkey" FOREIGN KEY ("vendor_id")
    REFERENCES "vendors" ("id") MATCH SIMPLE ON UPDATE NO ACTION ON DELETE NO ACTION
);

-- CREATE sequence "deals_id_seq";
-- alter table "deals" alter column "id" set default nextval('deals_id_seq');