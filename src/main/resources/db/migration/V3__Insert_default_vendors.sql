INSERT INTO vendors("url", "name", "select", "regex", "updated_at")
VALUES ('http://shop.oreilly.com', 'O''reilly', 'div[id=oreilly-deal]',
'.*<a.*href="(.*DEAL)".*\>\<img[\s]+alt=".*"[\s]+src="(.*)"[\s]+.*class=".*"\>\<\/a\>.*<a.*href=".*DEAL".*>(.*)<\/a>.*',
current_timestamp);

INSERT INTO vendors("url", "name", "select", "regex", "updated_at")
VALUES ('https://www.manning.com', 'Manning', 'em[class=deal-of-the-day-brief]',
'<em[\s]+class.*\>(.*)<\/em>',
current_timestamp);