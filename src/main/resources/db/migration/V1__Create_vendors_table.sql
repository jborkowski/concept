CREATE TABLE "vendors" (
  "id"         INTEGER PRIMARY KEY,
  "url"        VARCHAR NOT NULL,
  "name"       VARCHAR NOT NULL,
  "select"     VARCHAR NOT NULL,
  "regex"      VARCHAR NOT NULL,
  "updated_at" TIMESTAMP NOT NULL
);

CREATE SEQUENCE "vendors_id_seq";
ALTER TABLE "vendors" ALTER COLUMN "id" SET DEFAULT nextval('vendors_id_seq');