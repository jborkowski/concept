INSERT INTO vendors("url", "name", "select", "regex", "updated_at")
VALUES ('http://www.springer.com/', 'Springer', 'div[class=daily-deal-product]',
'.*<div[\s]+class="daily-deal-cover"\>.*?src="//(.*?)"[\s]+title="(.*?)".*',
current_timestamp);

INSERT INTO vendors("url", "name", "select", "regex", "updated_at")
VALUES ('http://www.informit.com', 'Informit', 'a[class=cover]',
'.*<a.*href="/deals.*"[\s]+class="cover">.*<img[\s]+src="(.*)"[\s]+alt="(.*?)"[\s]+.*',
current_timestamp);

INSERT INTO vendors("url", "name", "select", "regex", "updated_at")
VALUES ('http://www.peachpit.com', 'Peachpit', 'a[class=cover]',
'.*href="/deals.*".*<img src="/(.*)"[\s]+alt="(.*)"[\s]+.*',
current_timestamp);

INSERT INTO vendors("url", "name", "select", "regex", "updated_at")
VALUES ('http://www.apress.com/', 'Apress', 'div[class=daily-deal-cover]',
'.*<img[\s]+src="//(.*?)"[\s]+title="(.*?)".*',
current_timestamp);

INSERT INTO vendors("url", "name", "select", "regex", "updated_at")
VALUES ('http://www.ebookpoint.pl/', 'ebookpoint', 'img[alt=''Promocja dnia w ebookpoint.pl'']',
'.*<img[\s]+src="(.*?)"[\s]+.*alt="(.*?)".*',
current_timestamp);

