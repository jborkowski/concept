#!/bin/sh
# wait-for-postgres.sh

set -e

host="$(hostname --ip-address || echo '127.0.0.1')"
user="${POSTGRES_USER:-postgres}"
dbname="${POSTGRES_DB}"
export PGPASSWORD="${POSTGRES_PASSWORD:-}"

if select="$(echo 'SELECT 1' | psql --host "$host" --username "$user" --dbname "$dbname"  --quiet --no-align --tuples-only)" && [ "$select" = '1' ]; then
	echo 'OK'
fi
