logLevel := Level.Warn
addSbtPlugin("org.ensime" % "sbt-ensime" % "1.12.5")
addSbtPlugin("se.marcuslonnberg" % "sbt-docker" % "1.4.1")