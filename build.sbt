name := "coupons-fetcher"

organization := "com.jobo"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= {
  val akkaV       = "2.4.16"
  val akkaHttpV   = "10.0.1"
  val scalaTestV  = "3.0.1"
  val circeV = "0.7.0"
  Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaV,
    "com.typesafe.akka" %% "akka-stream" % akkaV,
    "com.typesafe.akka" %% "akka-testkit" % akkaV,
    "com.typesafe.akka" %% "akka-http" % akkaHttpV,
    "com.typesafe.akka" %% "akka-http-spray-json" % akkaHttpV,
    "com.typesafe.akka" %% "akka-http-testkit" % akkaHttpV,
    "org.scalatest"     %% "scalatest" % scalaTestV % "test",
    "org.scala-lang.modules" % "scala-xml_2.11" % "1.0.6",
    "de.heikoseeberger" %% "akka-http-circe" % "1.11.0",

    "com.typesafe.slick" % "slick_2.11" % "3.2.0",
    "org.slf4j" % "slf4j-nop" % "1.6.4",
    "com.typesafe.slick" % "slick-hikaricp_2.11" % "3.2.0",
    "org.flywaydb" % "flyway-core" % "3.2.1",
    "org.postgresql" % "postgresql" % "9.4-1201-jdbc41",

    "com.github.tminglei" %% "slick-pg" % "0.15.0-M4",
    "com.github.tminglei" %% "slick-pg_joda-time" % "0.15.0-M4",
    //"com.github.tminglei" %% "slick-pg_json4s" % "0.15.0-M4",
    "joda-time" % "joda-time" % "2.7",
    "org.joda" % "joda-convert" % "1.7",

    "io.circe" %% "circe-core" % circeV,
    "io.circe" %% "circe-generic" % circeV,
    "io.circe" %% "circe-parser" % circeV
  )
}
libraryDependencies += "org.jsoup" % "jsoup" % "1.10.2"

enablePlugins(DockerPlugin)

dockerfile in docker := {
  val jarFile = Keys.`package`.in(Compile, packageBin).value
  val classpath = (managedClasspath in Compile).value
  val mainclass = mainClass.in(Compile, packageBin).value.get
  val libs = "/app/libs"
  val jarTarget = "/app/" + jarFile.name
 // val appDir: File = stage.value

  new Dockerfile {
    // Use a base image that contain Java
    from("java")
    // Expose port 9000
    expose(9000)

    // Copy all dependencies to 'libs' in the staging directory
    classpath.files.foreach { depFile =>
      val target = file(libs) / depFile.name
      stageFile(depFile, target)
    }
    // Add the libs dir from the
    addRaw(libs, libs)

    //copy(s"$appDir/wait-for-postgres.sh", s"$libs/wait-for-postgres.sh")

    // Add the generated jar file
    add(jarFile, jarTarget)
    // The classpath is the 'libs' dir and the produced jar file
    val classpathString = s"$libs/*:$jarTarget"

    // entryPoint("./wait-for-postgres.sh")

    // Set the entry point to start the application using the main class
    cmd("java", "-cp", classpathString, mainclass)
  }
}
    